# Tack för att du delar ett potentiellt rekryteringsbehov!

Detta är din bekräftelse på att vi tagit emot behovet.

## Ärendenummer %{ISSUE_ID}
Vi har registrerat ett ärende och fördelar det till en handläggare inom 12 tim.
Du blir notifierad när statusen i ärendet förändras.

//Mvh
Enheten JobTech

Ps: Kolla gärna vilka andra [rekryteringbehov som finns](https://gitlab.com/Sodjs/case-handling/-/issues). Pga GDPR så kommer ditt ärende inte synas öppet innan vår community-manager har granskat ärendet.
